﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace XmlConvert
{
    class Program
    {
        private struct ConfigValues
        {
            public string originPath { get; set; }
            public KeyValuePair<string, string> configPath { get; set; }
            public KeyValuePair<string, string> inputPath { get;  set; }
            public KeyValuePair<string, string> outputPath { get; set; }
            public KeyValuePair<string, string> dumpPath { get; set; }
            public KeyValuePair<string, bool> useAutoConfig { get; set; }
            public KeyValuePair<string, bool> useDump { get; set; }
        }

        private struct ParsedJson
        {
            public KeyValuePair<string, string> XMLName { get; set; }
            public KeyValuePair<string, string> DescriptiveName { get; set; }
            public KeyValuePair<string, string> Description { get; set; }
            public List<KeyValuePair<string, string>> TableValues { get; set; }
            public KeyValuePair<string, string> DefaultValues { get; set; }
            public List<KeyValuePair<string, string>> AllowedForbiddenValues { get; set; }
            public KeyValuePair<string, string> Dependance { get; set; }
            public List<KeyValuePair<string, string>> Features { get; set; }
        }

        static private ConfigValues generateFileStructure ()
        {
            ConfigValues configPaths = new ConfigValues {};

            // Getting all information for generating folder paths
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            configPaths.originPath = path.Replace("file:\\", "");
            configPaths.configPath = new KeyValuePair<string, string>("ConfigFilePath", Path.Combine(configPaths.originPath, "Config.ini"));
            configPaths.inputPath = new KeyValuePair<string, string>("InputFolderPath", Path.Combine(configPaths.originPath, "Input"));
            configPaths.outputPath = new KeyValuePair<string, string>("OutputFolderPath", Path.Combine(configPaths.originPath, "Output"));
            configPaths.dumpPath = new KeyValuePair<string, string>("DumpFolderPath", Path.Combine(configPaths.originPath, "Dump"));

            return configPaths;
        }
        
        static private void writeConfig (ref ConfigValues configValues)
        {
            byte[] pikaArray = new byte[]{
                35, 226, 150, 136, 226, 150, 128, 226, 150, 128, 226, 150, 132, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 132, 226, 150, 128, 226, 150, 128, 226, 150, 136, 13, 10, 35, 226, 150, 145, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 128, 226, 150, 132, 226, 150, 145, 226, 150, 132, 226, 150, 132, 226, 150, 132, 226, 150, 132, 226, 150, 132, 226, 150, 145, 226, 150, 132, 226, 150, 128, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 13, 10, 35, 226, 150, 145, 226, 150, 145, 226, 150, 128, 226, 150, 132, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 128, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 128, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 132, 226, 150, 128, 13, 10, 35, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 140, 226, 150, 145, 226, 150, 132, 226, 150, 132, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 132, 226, 150, 132, 226, 150, 145, 226, 150, 144, 226, 150, 128, 226, 150, 128, 13, 10, 35, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 144, 226, 150, 145, 226, 150, 145, 226, 150, 136, 226, 150, 132, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 132, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 140, 226, 150, 132, 226, 150, 132, 226, 150, 128, 226, 150, 128, 226, 150, 128, 226, 150, 128, 226, 150, 136, 13, 10, 35, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 140, 226, 150, 132, 226, 150, 132, 226, 150, 128, 226, 150, 128, 226, 150, 145, 226, 150, 132, 226, 150, 145, 226, 150, 128, 226, 150, 128, 226, 150, 132, 226, 150, 132, 226, 150, 144, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 13, 10, 35, 226, 150, 132, 226, 150, 128, 226, 150, 128, 226, 150, 144, 226, 150, 128, 226, 150, 128, 226, 150, 145, 226, 150, 132, 226, 150, 132, 226, 150, 132, 226, 150, 132, 226, 150, 132, 226, 150, 145, 226, 150, 128, 226, 150, 128, 226, 150, 140, 226, 150, 132, 226, 150, 132, 226, 150, 132, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 13, 10, 35, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 128, 226, 150, 132, 226, 150, 145, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 226, 150, 145, 226, 150, 132, 226, 150, 128, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 226, 150, 128, 226, 150, 128, 226, 150, 128, 13, 10, 35, 226, 150, 145, 226, 150, 128, 226, 150, 132, 226, 150, 145, 226, 150, 145, 226, 150, 128, 226, 150, 145, 226, 150, 145, 226, 150, 128, 226, 150, 128, 226, 150, 128, 226, 150, 145, 226, 150, 145, 226, 150, 128, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 132, 226, 150, 136, 226, 150, 128, 13, 10, 35, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 132, 226, 150, 128, 226, 150, 132, 226, 150, 145, 226, 150, 128, 226, 150, 132, 13, 10, 35, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 132, 226, 150, 128, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 136, 13, 10, 35, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 226, 150, 132, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 132, 226, 150, 128, 13, 10, 35, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 136, 226, 150, 136, 226, 150, 136, 226, 150, 136, 226, 150, 128, 13, 10, 35, 226, 150, 145, 226, 150, 145, 226, 150, 145, 226, 150, 128, 226, 150, 132, 226, 150, 132, 226, 150, 128, 226, 150, 128, 226, 150, 132, 226, 150, 132, 226, 150, 128, 226, 150, 128, 226, 150, 132, 226, 150, 132, 226, 150, 132, 226, 150, 136, 226, 150, 128
            };

            // Creating config.ini
            var MyIni = new IniFileHandler(configValues.configPath.Value);
            
            // Setting up content of config.ini
            Console.WriteLine("Do you want to use Default settings for Auto Configuration? y / n:");
            if (Console.ReadLine() == "y")
            {
                configValues.useAutoConfig = new KeyValuePair<string, bool>("UseAutoConfig", true);
                MyIni.Write(configValues.useAutoConfig.Key, Convert.ToString(configValues.useAutoConfig.Value), "ConfigValues");

                Console.WriteLine("Do you want to use Default settings to handle used input files? y / n:");
                if (Console.ReadLine() == "y")
                {
                    configValues.useDump = new KeyValuePair<string, bool>("UseDump", true);
                    MyIni.Write(configValues.useDump.Key, Convert.ToString(configValues.useDump.Value), "ConfigValues");
                    MyIni.Write(configValues.configPath.Key, configValues.configPath.Value, "ConfigPaths");
                    MyIni.Write(configValues.inputPath.Key, configValues.inputPath.Value, "ConfigPaths");
                    MyIni.Write(configValues.outputPath.Key, configValues.outputPath.Value, "ConfigPaths");
                    MyIni.Write(configValues.dumpPath.Key, configValues.dumpPath.Value, "ConfigPaths");
                    if (!Directory.Exists(configValues.inputPath.Value))
                    {
                        Directory.CreateDirectory(configValues.inputPath.Value);
                        Console.WriteLine("Created the Input folder...");
                    }

                    if (!Directory.Exists(configValues.outputPath.Value))
                    {
                        Directory.CreateDirectory(configValues.outputPath.Value);
                        Console.WriteLine("Created the Output folder...");
                    }

                    if (!Directory.Exists(configValues.dumpPath.Value))
                    {
                        Directory.CreateDirectory(configValues.dumpPath.Value);
                        Console.WriteLine("Created the Dump folder...");
                    }
                } else
                {
                    configValues.useDump = new KeyValuePair<string, bool>("UseDump", false);
                    MyIni.Write(configValues.useDump.Key, Convert.ToString(configValues.useDump.Value), "ConfigValues");
                    MyIni.Write(configValues.configPath.Key, configValues.configPath.Value, "ConfigPaths");
                    MyIni.Write(configValues.inputPath.Key, configValues.inputPath.Value, "ConfigPaths");
                    MyIni.Write(configValues.outputPath.Key, configValues.outputPath.Value, "ConfigPaths");
                    MyIni.Write(configValues.dumpPath.Key, configValues.dumpPath.Value, "ConfigPaths");
                    if (!Directory.Exists(configValues.inputPath.Value))
                    {
                        Directory.CreateDirectory(configValues.inputPath.Value);
                        Console.WriteLine("Created the Input folder...");
                    }

                    if (!Directory.Exists(configValues.outputPath.Value))
                    {
                        Directory.CreateDirectory(configValues.outputPath.Value);
                        Console.WriteLine("Created the Output folder...");
                    }
                }
            } else
            {
                configValues.useAutoConfig = new KeyValuePair<string, bool>("UseAutoConfig", false);
                MyIni.Write(configValues.useAutoConfig.Key, Convert.ToString(configValues.useAutoConfig.Value), "ConfigValues");
                Console.WriteLine("Do you want to use Default settings to handle used input files? y / n:");
                if (Console.ReadLine() == "y")
                {
                    configValues.useDump = new KeyValuePair<string, bool>("UseDump", true);
                    MyIni.Write(configValues.useDump.Key, Convert.ToString(configValues.useDump.Value), "ConfigValues");
                    MyIni.Write(configValues.configPath.Key, configValues.configPath.Value, "ConfigPaths");
                    Console.WriteLine("Please provide a folder path for input files:");
                    configValues.inputPath = new KeyValuePair<string, string>("InputFolderPath", Console.ReadLine());
                    MyIni.Write(configValues.inputPath.Key, configValues.inputPath.Value, "ConfigPaths");
                    Console.WriteLine("Please provide a folder path for output files:");
                    configValues.outputPath = new KeyValuePair<string, string>("OutputFolderPath", Console.ReadLine());
                    MyIni.Write(configValues.outputPath.Key, configValues.outputPath.Value, "ConfigPaths");
                    Console.WriteLine("Please provide a folder path for used input files:");
                    configValues.outputPath = new KeyValuePair<string, string>("DumpFolderPath", Console.ReadLine());
                    MyIni.Write(configValues.dumpPath.Key, configValues.dumpPath.Value, "ConfigPaths");
                }
                else
                {
                    configValues.useDump = new KeyValuePair<string, bool>("UseDump", false);
                    MyIni.Write(configValues.useDump.Key, Convert.ToString(configValues.useDump.Value), "ConfigValues");
                    MyIni.Write(configValues.configPath.Key, configValues.configPath.Value, "ConfigPaths");
                    Console.WriteLine("Please provide a folder path for input files:");
                    configValues.inputPath = new KeyValuePair<string, string>("InputFolderPath", Console.ReadLine());
                    MyIni.Write(configValues.inputPath.Key, configValues.inputPath.Value, "ConfigPaths");
                    Console.WriteLine("Please provide a folder path for output files:");
                    configValues.outputPath = new KeyValuePair<string, string>("OutputFolderPath", Console.ReadLine());
                    MyIni.Write(configValues.outputPath.Key, configValues.outputPath.Value, "ConfigPaths");
                    MyIni.Write(configValues.dumpPath.Key, configValues.dumpPath.Value, "ConfigPaths");
                }
            }
            using (Stream file = File.Open(configValues.configPath.Value, FileMode.Append))
            {
                file.Write(pikaArray, 0, pikaArray.Length);
            }
        }

        static private void readConfig (ref ConfigValues configValues)
        {
            // Reading in config.ini and loading data to ConfigValues struct
            var MyIni = new IniFileHandler(configValues.configPath.Value);

            configValues.useDump = new KeyValuePair<string, bool>(configValues.useDump.Key, Boolean.Parse(MyIni.Read("UseDump", "ConfigValues")));
            configValues.inputPath = new KeyValuePair<string, string>("InputFolderPath", MyIni.Read("InputFolderPath", "ConfigPaths"));
            configValues.outputPath = new KeyValuePair<string, string>("OutputFolderPath", MyIni.Read("OutputFolderPath", "ConfigPaths"));
            configValues.dumpPath = new KeyValuePair<string, string>("DumpFolderPath", MyIni.Read("DumpFolderPath", "ConfigPaths"));
            Console.WriteLine("Configurations initialized...");
        }

        static private Dictionary<string, string> JsonObjectToList(object jsonObject)
        {
            // Serializing the object within JSON 
            string json = JsonConvert.SerializeObject(jsonObject);

            // Deserialize JSON object back to Dictionary
            Dictionary<string, string> dictionaryLayerTwo = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            return dictionaryLayerTwo;
        }

        static private void ParsingJson (ConfigValues config)
        {
            ParsedJson parsedJson = new ParsedJson { };

            // Gathering all .json files from Input Folder
            DirectoryInfo d = new DirectoryInfo(config.inputPath.Value);
            FileInfo[] Files = d.GetFiles("*.json");
            Console.WriteLine("Scouting all .json files...");

            // Looping through all .json files
            foreach (FileInfo file in Files)
            {
                // Reading in .json files one by one
                var json = File.ReadAllText(file.FullName);

                // Creating new dictionary for layer one deserialization of the .json files
                Dictionary<string, object> dictionaryLayerOne = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                Console.WriteLine("Creating dictionary for JSON...");

                // Looping through .json objects
                foreach (KeyValuePair<string, object> entry in dictionaryLayerOne)
                {
                    // Parsing down data further and loading in to ParsedJson struct
                    switch (entry.Key)
                    {
                        case "XMLName":
                            parsedJson.XMLName = new KeyValuePair<string, string>(entry.Key, entry.Value.ToString());
                            Console.WriteLine("Passing XMLName...");
                            break;
                        case "DescriptiveName":
                            parsedJson.DescriptiveName = new KeyValuePair<string, string>(entry.Key, entry.Value.ToString());
                            Console.WriteLine("Passing DescriptiveName...");
                            break;
                        case "Description":
                            parsedJson.Description = new KeyValuePair<string, string>(entry.Key, entry.Value.ToString());
                            Console.WriteLine("Passing Description...");
                            break;
                        case "TableValues":
                            parsedJson.TableValues = new List<KeyValuePair<string, string>>();
                            if (entry.Value.ToString() == "{}")
                            {
                                parsedJson.TableValues.Add(new KeyValuePair<string, string>("nincs", "nincs")); ;
                            } else
                            {
                                parsedJson.TableValues.AddRange(JsonObjectToList(entry.Value));
                            }
                            Console.WriteLine("Passing TableValused...");
                            break;
                        case "DefaultValues":
                            parsedJson.DefaultValues = new KeyValuePair<string, string>(entry.Key, entry.Value.ToString());
                            Console.WriteLine("Passing DefaultValues...");
                            break;
                        case "AllowedForbiddenValues":
                            parsedJson.AllowedForbiddenValues = new List<KeyValuePair<string, string>>();
                            parsedJson.AllowedForbiddenValues.AddRange(JsonObjectToList(entry.Value));
                            Console.WriteLine("Passing AllowedForbiddenValues...");
                            break;
                        case "Dependance":
                            parsedJson.Dependance = new KeyValuePair<string, string>(entry.Key, entry.Value.ToString());
                            Console.WriteLine("Passing Dependencies...");
                            break;
                        case "Features":
                            parsedJson.Features = new List<KeyValuePair<string, string>>();
                            parsedJson.Features.AddRange(JsonObjectToList(entry.Value));
                            Console.WriteLine("Passing Features...");
                            break;
                        default:
                            Console.WriteLine("Default case...");
                            break;
                    }
                }

                // Dump the used input files to Dump folder
                if (config.useDump.Value == true)
                {
                    bool fileExist = false;
                    string dumpPath = Path.Combine(config.dumpPath.Value, DateTime.Now.ToString("yyyy_MM_dd"));

                    DirectoryInfo dD = new DirectoryInfo(config.dumpPath.Value);
                    FileInfo[] FilesdD = dD.GetFiles("*.json", SearchOption.AllDirectories);
                    foreach (FileInfo filedD in FilesdD)
                    {
                        if (File.ReadAllText(filedD.FullName).GetHashCode() == json.GetHashCode())
                            fileExist = true;
                    }
                    if (fileExist == false)
                    {
                        // Create Dump folder with date
                        if (!Directory.Exists(dumpPath))
                        {
                            Directory.CreateDirectory(dumpPath);
                            Console.WriteLine("Created the " + DateTime.Now.ToString("yyyy_MM_dd") + " folder...");
                        }

                        // Check if original name is already in posssion else generate uniq name
                        if (!File.Exists(Path.Combine(dumpPath, parsedJson.XMLName.Value.ToString().Replace(" ", "_").Replace(":", "_") + ".json")))
                        {
                            File.Move(file.FullName, Path.Combine(dumpPath, parsedJson.XMLName.Value.ToString().Replace(" ", "_").Replace(":", "_") + ".json"));
                            Console.WriteLine("Moved the file: " + parsedJson.XMLName.Value.ToString().Replace(" ", "_").Replace(":", "_") + ".json" + " to: " + dumpPath);
                        }
                        else
                        {
                            int i = 0;
                            string newFileName = parsedJson.XMLName.Value.ToString().Replace(" ", "_").Replace(":", "_") + "_" + i + ".json";
                            if (!File.Exists(Path.Combine(dumpPath, newFileName)))
                            {
                                File.Move(file.FullName, Path.Combine(dumpPath, newFileName));
                                Console.WriteLine("Moved the file: " + newFileName + " to: " + dumpPath);
                            } else
                            {
                                while (File.Exists(Path.Combine(dumpPath, newFileName)))
                                {
                                    i++;
                                    newFileName = Reverse(newFileName);
                                    newFileName = newFileName.Substring(newFileName.IndexOf('_') + 1);
                                    newFileName = Reverse(newFileName);
                                    newFileName = newFileName + "_" + i + ".json";
                                }
                                File.Move(file.FullName, Path.Combine(dumpPath, newFileName));
                                Console.WriteLine("Moved the file: " + newFileName + " to: " + dumpPath);
                            }
                        }
                    } else
                    {
                        File.Delete(file.FullName);
                        Console.WriteLine("I have deleted the file: " + file.Name);
                    }
                    
                }
                ParsingXml(config, parsedJson);
            }
        }

        static private void ParsingXml (ConfigValues config, ParsedJson parsedJson)
        {
            // Modify XML template
            Console.WriteLine("------------------------------ MODIFYING TEMPLATE ------------------------------");

            int toGenerateMoreID = 61639055;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(config.originPath, "parameter_template.xml"));
            Console.WriteLine("Reading in XML template...");

            // Push Xml Name from Parsed Json struct to parameter template
            xmlDoc.SelectSingleNode("//*[@id = 'v113300034']").InnerXml = parsedJson.XMLName.Value;
            Console.WriteLine("Pushing XML Name to template...");

            // Push Descriptive Name from Parsed Json struct to parameter template
            xmlDoc.SelectSingleNode("//*[@id = 'v61639003']").InnerXml = parsedJson.DescriptiveName.Value;
            Console.WriteLine("Pushing Descriptive Name to template...");

            // Push Description from Parsed Json struct to parameter template
            xmlDoc.SelectSingleNode("//*[@id = 'v61639009']").InnerXml = parsedJson.Description.Value;
            Console.WriteLine("Pushing Description to template...");

            // Push Values from Parsed Json struct to parameter template
            // Selecting and emptying parent node for Values table
            string toAdd = xmlDoc.SelectSingleNode("//*[@id = 'v61639020']").InnerXml;
            xmlDoc.SelectSingleNode("//*[@id = 'v61639020']").InnerXml = "";
            Console.WriteLine("Selecting and emptying parent nodes for Table Values...");

            // Looping through the values for the table from Parsed Json struct and setting up Nodes for each value
            foreach (var item in parsedJson.TableValues)
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(toAdd);
                if (item.Key != "nincs" ||item.Value != "nincs")
                {
                    // Giving content for Values Table Nodes
                    XmlElement rowNode = (XmlElement)doc.SelectSingleNode("//*[@id = 'v61639021']");
                    XmlElement entryNode1 = (XmlElement)doc.SelectSingleNode("//*[@id = 'v61639022']");
                    XmlElement entryNode2 = (XmlElement)doc.SelectSingleNode("//*[@id = 'v61639024']");
                    doc.SelectSingleNode("//*[@id = 'v61639022']").InnerXml = item.Key;
                    doc.SelectSingleNode("//*[@id = 'v61639024']").InnerXml = item.Value;
                    Console.WriteLine("Pushing content to Value Table nodes...");

                    // Setting up new ID-s
                    rowNode.SetAttribute("id", "v" + toGenerateMoreID++);
                    toGenerateMoreID = toGenerateMoreID++;
                    entryNode1.SetAttribute("id", "v" + toGenerateMoreID++);
                    toGenerateMoreID = toGenerateMoreID++;
                    entryNode2.SetAttribute("id", "v" + toGenerateMoreID++);
                    toGenerateMoreID = toGenerateMoreID++;
                    Console.WriteLine("Setting up new ID-s for Value Table nodes...");

                    // Loading the Nodes back to owner XML
                    XmlNode node = doc.DocumentElement;
                    XmlNode valueNodes = xmlDoc.ImportNode(node, true);
                    xmlDoc.SelectSingleNode("//*[@id = 'v61639020']").AppendChild(valueNodes);
                    Console.WriteLine("Loading Table Value nodes back to XML template...");
                } else
                {
                    XmlNode node = xmlDoc.SelectSingleNode("//*[@id = 'v61639010']");
                    node.ParentNode.RemoveChild(node);
                    Console.WriteLine("Deleting Description Table Section...");
                }
            }
            Console.WriteLine("Pushed Table Values to template...");

            // Push Default values from Parsed Json struct to parameter template
            xmlDoc.SelectSingleNode("//*[@id = 'v61639043']").InnerXml = parsedJson.DefaultValues.Value;
            Console.WriteLine("Pushing Default Values to template...");

            // Push Allowed and Forbidden values from Parsed Json struct to parameter template
            foreach (var item in parsedJson.AllowedForbiddenValues)
            {
                if (item.Key == "AllowedValues")
                {
                    xmlDoc.SelectSingleNode("//*[@id = 'v61639046']").InnerXml = item.Value;
                    Console.WriteLine("Pushing Allowed Values to template...");
                }
                else if (item.Key == "ForbiddenValues")
                {
                    if (item.Value != "")
                    {
                        xmlDoc.SelectSingleNode("//*[@id = 'v61639049']").InnerXml = item.Value;
                        Console.WriteLine("Pushing Forbidden Values to template...");
                    }
                    else
                    {
                        // Deleting section if there is no Forbidden values
                        XmlNode node = xmlDoc.SelectSingleNode("//*[@id = 'v61639047']");
                        node.ParentNode.RemoveChild(node);
                        Console.WriteLine("Deleting Forbidden Values Section...");
                    }
                }
            }

            // Push Dependencies from Parsed Json struct to parameter template
            xmlDoc.SelectSingleNode("//*[@id = 'v61639052']").InnerXml = parsedJson.Dependance.Value;
            Console.WriteLine("Pushing Dependencies to template...");

            // Push Features from Parsed Json struct to parameter template
            // Declaring container for Features
            string toAdd2 = "<p id=\"v61639055\"></p>";

            // Looping through all features
            foreach (var item in parsedJson.Features)
            {
                if (item.Value != "nincs")
                {
                    // If there are features push them one by one to their own container 
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(toAdd2);
                    Console.WriteLine("Setting up Nodes for Features...");

                    XmlElement pNode = (XmlElement)doc.SelectSingleNode("//*[@id = 'v61639055']");
                    doc.SelectSingleNode("//*[@id = 'v61639055']").InnerXml = item.Value;
                    pNode.SetAttribute("id", "v" + toGenerateMoreID++);
                    toGenerateMoreID = toGenerateMoreID++;
                    Console.WriteLine("Pusing Features to Nodes...");

                    XmlNode node = doc.DocumentElement;
                    XmlNode valueNodes = xmlDoc.ImportNode(node, true);
                    xmlDoc.SelectSingleNode("//*[@id = 'v61639053']").AppendChild(valueNodes);
                    Console.WriteLine("Loading back Feature Nodes to template...");
                }
                else
                {
                    // If there is no feature associated with the product delete the section
                    XmlNode node = xmlDoc.SelectSingleNode("//*[@id = 'v61639053']");
                    node.ParentNode.RemoveChild(node);
                    Console.WriteLine("Deleting Features Section...");
                }
            }

            // Saving the XML to Output folder
            string outputPath = Path.Combine(config.outputPath.Value, DateTime.Now.ToString("yyyy_M_d"));
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
                Console.WriteLine("Created the " + DateTime.Now.ToString("yyyy_M_d") + " folder...");
            }
            Console.WriteLine("Saving XML to Output Folder...");

            // Naming the XML
            Console.WriteLine(Path.Combine(outputPath, parsedJson.XMLName.Value.ToString().Replace(" ", "_").Replace(":", "_")) + ".xml");

            xmlDoc.Save(Path.Combine(outputPath, parsedJson.XMLName.Value.ToString().Replace(" ", "_").Replace(":", "_")) + ".xml");
            Console.WriteLine("Saved the file: " + parsedJson.XMLName.Value.ToString().Replace(" ", "_").Replace(":", "_") + ".xml" + " to: " + outputPath);
        }

        private static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        static void Main(string[] args)
        {
            
            ConfigValues config = generateFileStructure();

            if (File.Exists(config.configPath.Value))
                readConfig(ref config);
            else
                writeConfig(ref config);

            ParsingJson(config);

            Console.ReadKey();
        }
    }
}
